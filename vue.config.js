// файл vue.config.js расположен в корне вашего репозитория
module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',
    transpileDependencies: [
        'vuetify'
    ],
    devServer: {
        proxy: {
            '/api': {
                target: 'http://178.154.199.253/dsks-cmw', //'http://dskscmw-dev.web.local/dsks-cmw', //'http://localhost:8080/(APPLICATION_CONTEXT - если есть)'
                ws: true,
                changeOrigin: true
            }
        }
    }
}
